const functions = require("firebase-functions");
const express = require("express");
const cors = require("cors");
/* eslint-disable */ 
const {response} = require("express");
/* eslint-disable */ 
const stripe = require("stripe")(
    "sk_test_51JnG7GSIyNGa5l5PBr6IN0rB6pY1CsCfEPpRWngwbtJpzjX1km83zG9rBonTxSbLIWkGMRheY2MXEfyaS7mgUOD3004tc3aF22");


// API


// -App config
const app = express();

// -Middlewares
app.use(cors({origin: true}));
app.use(express.json());

// -API routes
app.get("/", ( request, response)=> response.status(200).send("hello world"));
app.post("/payments/create", async (request, response) => {
  const total = request.query.total;
  console.log("Payment request received", total);

  const paymentIntent = await stripe.paymentIntents.create({
    amount: total, // subunits of the currency
    currency: "INR",
  });
  response.status(201).send({
    clientSecret: paymentIntent.client_secret,
  });
});

// -Listen command
exports.api = functions.https.onRequest(app);

