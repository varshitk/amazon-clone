// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyBZXWfE5BE-f7JLx891YsiRFVyifH8ywQI",
    authDomain: "clone-8fee2.firebaseapp.com",
    projectId: "clone-8fee2",
    storageBucket: "clone-8fee2.appspot.com",
    messagingSenderId: "766391997890",
    appId: "1:766391997890:web:4b0c5f84abf44fbf6a725b",
    measurementId: "G-BJXBTYC1YK"
  };

 const firebaseApp = firebase.initializeApp(firebaseConfig); 

 const db = firebaseApp.firestore();
 const auth = firebase.auth();

 export{db , auth};